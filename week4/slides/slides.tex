\documentclass{beamer}

\usetheme{Singapore}

\usepackage{fancyvrb}
\usepackage{fontspec}
\usepackage{relsize}

\newfontfamily{\korean}{UnBatang}

\title{Introduction to Text Processing}
\author[Kilian Evang]{Kilian Evang}
\date{2017-02-28}

\begin{document}

\begin{frame}
 \titlepage
\end{frame}

\begin{frame}
 \frametitle{Outline}
 \tableofcontents
\end{frame}

\section{Text corpora}

\subsection{Text corpora}

\begin{frame}
 \frametitle{Natural language processing (NLP)}
 \begin{itemize}
  \item The process of a computer extracting meaningful information from
        natural language output.
  \item Powerful Python libraries available: NLTK, spaCy...
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Text corpora}
 \begin{itemize}
  \item a \emph{corpus} (plural \emph{corpora}) is a collection of texts in
        digital format
  \item in linguistics, used as source of quantitative and qualitative data
        about language use
  \item in NLP, used to train statistical algorithms and to test them
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Types of corpora}
 \begin{itemize}
  \item open-domain vs. specific domain
  \item monolingual vs. multilingual
  \item modern vs. historical
  \item written vs. speech
  \item annotated vs. unannotated
  \item ...
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Annotation}
 Corpora are often \emph{annotated} with linguistic information of the
 kind that NLP is ultimately supposed to find automatically from pure text,
 e.g.
 \begin{itemize}
  \item part-of-speech (e.g. noun vs. verb)
  \item word sense (e.g. wood the material vs. wood the place)
  \item sentiment (positive or negative attitude expressed in a text)
  \item entities (e.g. link each word in a text to the Wikipedia page describing
        the concept)
  \item syntactic structures
  \item semantic structures
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Annotation -- Example}
  \begin{Verbatim}[commandchars=\\\{\}]

     The brown dog is in the park. 
     DT  JJ    NN  VB IN DT  NN
  \end{Verbatim}
  \pause

  \includegraphics[height=4.5cm]{gmb.png}
\end{frame}

\begin{frame}
 \frametitle{Text processing}
 \begin{itemize}
  \item a set of programming techniques dealing with text
  \item of fundamental importance in
  \begin{itemize}
   \item creating, maintaining and using text corpora
   \item natural language processing
   \item search engines
   \item data mining, e.g. for Web advertising
   \item ...
  \end{itemize}
 \end{itemize}
\end{frame}

\section{Computers and Text}

\subsection{Characters}

\begin{frame}
 \frametitle{Characters}
 \begin{itemize}
  \item Python's \texttt{str} datatype is for \emph{sequences of characters}
  \item But what is a character?
  \begin{itemize}
   \item in computing: smallest unit of which text is composed
   \item today, computers need to deal with a lot of different characters
  \end{itemize}
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Examples of characters (1/2)}
 \begin{itemize}
  \item Latin letters: A, B, C, a, b, c...
  \item Digits: 0, 1, 2...
  \item Punctuation and symbols: ., !., (, ), \%, €...
  \item space
  \item newline
  \item ...
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Examples of characters (2/2)}
 \begin{columns}
  \column{0.5\textwidth}
   \includegraphics[height=7cm]{scrabble.png}
  \column{0.5\textwidth}
   \begin{itemize}
    \item Greek letters
    \item Chinese characters
    \item ligatures
    \item Cyrillic letters
    \item Hebrew letters
    \item Thai letters
    \item Mayan numerals
    \item ...
   \end{itemize}
 \end{columns}
\end{frame}

\begin{frame}
 \frametitle{What is a character?}
 Sometimes it's a matter of definition.\pause
 \begin{itemize}
  \item Characters with diacritics
  \begin{itemize}
   \item Is \~a one character?
   \item Is it the combination of two characters, a and \textasciitilde?
   \item Unicode standard supports both views.
  \end{itemize}\pause
  \item Korean writing system
  \begin{itemize}
   \item consists of letters: {\korean ㅇㅏㄴ}...
   \item but they are written in syllable blocks: {\korean 안녕하세요}...
   \item 24 letters, but > 11,000 possible blocks
   \item Unicode standard: each block has its own character
  \end{itemize}
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Unicode}
 \begin{itemize}
  \item standard created in 1991
  \item defines a Universal Character Set that aims to cover
  \begin{itemize}
   \item all of the world's languages/writing systems
   \item including historical and extinct characters
   \item common symbols from all branches of sciences, arts and technology
  \end{itemize}
  \item \url{http://www.cl.cam.ac.uk/~mgk25/ucs/examples/UTF-8-demo.txt}
  \item assigns each character a unique number called a \emph{codepoint}
  \item 1.1 million codepoints available, 110,000 currently assigned
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{\texttt{str} revisited}
 \begin{itemize}
  \item thus, a \texttt{str} is essentially a sequence of numbers (codepoints)
  \item Python provides a special format to write characters directly as
        numbers, in hexadecimal form:
 \end{itemize}

\texttt{>>> '\textbackslash u0048\textbackslash u0065\textbackslash u006c\textbackslash u006c\textbackslash u006f\textbackslash u0021\textbackslash u0020\textbackslash uc548\\\textbackslash ub155\textbackslash ud558\textbackslash uc138\textbackslash uc694\textbackslash uff01'}\\
\texttt{'Hello! {\korean 안녕하세요！}'}
\end{frame}

\subsection{Text files}

\begin{frame}[fragile]
 \frametitle{Text files (1/2)}
 \begin{itemize}
  \item a file contains a sequence of 0's and 1's (bits)
  \item they come in groups of 8 bits (= 1 byte)
  \item e.g.
 \end{itemize}
 \begin{verbatim}
01001000 01100101 01101100 01101111 00100001 00100000
 \end{verbatim}
\end{frame}

\begin{frame}
 \frametitle{Text files (2/2)}
 \begin{itemize}
  \item How to put characters into files?\pause
  \item 1 character = 1 byte?
  \begin{itemize}
   \item used to be a common approach
   \item there are only $2^8=256$ possible bytes
   \item not enough for Unicode (1.1 million codepoints)
  \end{itemize}\pause
  \item 1 character = 4 bytes?
  \begin{itemize}
   \item $2^{32}=4,294,967,296$ possible 4-byte combinations -- definitely
         enough!
   \item but text files would get quite big
  \end{itemize}
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{UTF-8}
 \begin{itemize}
  \item UTF-8 is the most common encoding for text today
  \item it is a \emph{variable-length} encoding:
  \begin{itemize}
   \item codepoints 0--127 (ASCII) use \textbf{one byte}
   \item codepoints 128--2,047 use \textbf{two bytes}
   \item codepoints 2,048--65,535 use \textbf{three bytes}
   \item higher codepoints use \textbf{four bytes}
  \end{itemize}
  \item generally, more commonly used characters have lower codepoints, thus
        need fewer bytes
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Other notable encodings}
 \begin{itemize}
  \item ASCII: can only handle the first 128 characters
  \item Latin-1 aka ISO-8859-1: can only handle the ASCII characters plus
        the 128 most important additional characters for Western-European
        languages
  \item Windows-1252: slight variant of Latin-1, often confused
  \item lots of other 256-character ASCII extensions for specific languages
  \item UTF-16: old encoding for Unicode, uses two bytes, can only handle
        65,536 characters smoothly, uses ugly tricks for handling more
        characters, default encoding on Windows
  \item bottom line: use UTF-8 whenever possible, but be aware that not all
        text files you will encounter are in UTF-8
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Encoding strings in Python}
 \texttt{>>> 'ã'.encode('UTF-8')}\\
 \texttt{b'\textbackslash xc3\textbackslash xa3'}\\
 \texttt{>>> '{\korean 안}'.encode('UTF-8')}\\
 \texttt{b'\textbackslash xec\textbackslash x95\textbackslash x88'}
 \begin{itemize}
  \item data type \texttt{bytes} for sequences of bytes
  \item examples show variable number of bytes per character
  \item \texttt{decode} method converts \texttt{bytes} to \texttt{str}
  \item when writing and reading files, encoding and decoding is automatic,
        but you \textbf{have} to know the encoding of your file
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{What happens if you write/read with different encodings}
 \begin{Verbatim}
with open('myfile.txt', 'w', encoding='UTF-8') \
        as myfile:
    myfile.write('Düsseldorf')

with open('myfile.txt', 'r', encoding='Latin-1') \
        as myfile:
    print(myfile.readline())

DÃ¼sseldorf
 \end{Verbatim}
\end{frame}

\begin{frame}[fragile]
 \frametitle{What happens if you write/read with different encodings}
 \begin{Verbatim}
>>> with open('myfile.txt', 'w', encoding='Latin-1') \
...        as myfile:
...    myfile.write('Düsseldorf')
>>> with open('myfile.txt', 'r', encoding='UTF-8') \
...        as myfile:
...    print(myfile.readline())

Traceback (most recent call last):
  File "<stdin>", line 3, in <module>
  File "/usr/lib/python3.2/codecs.py", line 300, in decode
    (result, consumed) = 
       self._buffer_decode(data, self.errors, final)
UnicodeDecodeError: 'utf-8' codec can't decode byte 0xfc
in position 1: invalid start byte
 \end{Verbatim}
\end{frame}

\section{Tokenization}

\subsection{Tokenization}

\begin{frame}[fragile]
 \frametitle{Tokenization}
 \begin{itemize}
  \item task: splitting a string up into \emph{tokens}
  \item tokens are 1) words, 2) punctuation marks, 3) various other things
  \item first and most basic step in natural language processing, should be
        done automatically
  \item various algorithms and tools exist, none is perfect
  \item Where to split text? Most important cues: whitespace, punctuation
 \end{itemize}
 \begin{Verbatim}
>>> tokenize('"Hark! A vagrant," he said.')
['"', 'Hark', '!', 'A', 'vagrant', ',', '"', 'he',
'said', '.']
 \end{Verbatim}
\end{frame}

\begin{frame}
 \frametitle{Tricky cases in tokenization}
 \begin{itemize}
  \item English compound nouns: is \emph{prime minister} one token or two
        tokens?
  \item Names: is \emph{New York} one or two tokens?
  \item Haplographs: in the sentence \emph{She was chairwoman of Lorillard
        Inc.}, is the period part of the acronym or a sentence-ending period?
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Tokenization in Python}
 A first attempt:
 \smaller\smaller
 \begin{Verbatim}
>>> with open('a_hard_days_night.txt') as song:
...     content = song.read()
>>> print(content)
It's been a hard day's night, and i been working like a dog
[...]
>>> print(content.split(' '))
["It's", 'been', 'a', 'hard', "day's", 'night,', 'and', 'i', 'been', 'working',
'like', 'a', "dog\nIt's", 'been', 'a', 'hard', "day's", 'night,', 'i', 'should',
'be', 'sleeping', 'like', 'a', 'log\nBut', 'when', 'i', 'get', 'home', 'to',
'you', "i'll", 'find', 'the', 'things', 'that', 'you', 'do\nWill', 'make', 'me',
'feel', 'alright\n\nYou', 'know', 'i', 'work', 'all', 'day', 'to', 'get', 'you',
'money', 'to', 'buy', 'you', 'things\nAnd', "it's", 'worth', 'it', 'just', 'to',
'hear', 'you', 'say', "you're", 'going', 'to', 'give', 'me', 'everything\nSo',
'why', 'on', 'earth', 'should', 'i', 'moan,', "'cause", 'when', 'i', 'get',
'you', 'alone\nYou', 'know', 'i', 'feel', 'ok\n\nWhen', "i'm", 'home',
'everything', 'seems', 'to', 'be', 'right\nWhen', "i'm", 'home', 'feeling',
'you', 'holding', 'me', 'tight,', 'tight\n\nOwww!\n\nSo', 'why', 'on', 'earth',
'should', 'i', 'moan,', "'cause", 'when', 'i', 'get', 'you', 'alone\nYou',
'know', 'i', 'feel', 'ok\n\nYou', 'know', 'i', 'feel', 'alright\nYou', 'know',
'i', 'feel', 'alright\n\n']
 \end{Verbatim}
\end{frame}

%\subsection{Sentence segmentation}

%\begin{frame}
% \frametitle{Sentence segmentation}
% \begin{itemize}
%  \item task: in running text, find where one sentence ends and the next
%        begins
%  \item most important cues: sentence-final punctuation (?!.), capitalization
% \end{itemize}
%\end{frame}

%\begin{frame}
% \frametitle{Tricky cases in sentence segmentation}
% \begin{itemize}
%  \item period is not only used to end a sentence, but also for abbreviations
%  \item look at whether the next word is capitalized, but it might also be
%        a name: \emph{Chairwoman of Lorillard Inc\textbf{. L}oretta Miller said
%        yesterday...}
% \end{itemize}
%\end{frame}

\section{Counting words}

\subsection{Counting words}

\begin{frame}
 \frametitle{Counting words}
 \begin{itemize}
  \item another basic technique: counting how often each word appears in a
        document/in a collection of documents
  \item the most frequent words (words that appear most often) are not the
        most informative ones
  \item 100 most frequent words from Project Gutenberg:
 \end{itemize}
 \emph{a, about, after, all, and, any, an, are, as, at, been, before, be, but, by, can,
could, did, down, do, first, for, from, good, great, had, has, have, her, he,
him, his, if, into, in, is, its, it, I, know, like, little, made, man, may, men, me,
more, Mr, much, must, my, not, now, no, of, on, one, only, or, other, our,
out, over, said, see, she, should, some, so, such, than, that, the, their, them,
then, there, these, they, this, time, to, two, upon, up, us, very, was, were, we,
what, when, which, who, will, with, would, you, your}
\end{frame}

\begin{frame}
 \frametitle{Zipf's law}
 \center
 \includegraphics[height=6cm]{zipf.png}\\
 \url{http://www.ucl.ac.uk/~ucbplrd/language\_page.htm}
\end{frame}

\begin{frame}
 \frametitle{Word frequencies}
 \begin{itemize}
  \item Zipf's law for natural language: a few words are disproportionately more
        frequent than other words
  \item the most frequent words are \emph{function words} (conjunctions,
        articles, prepositions, pronouns...)
  \item each \emph{content word} (nouns, verbs, adjectives, adverbs...) is 
        typically less frequent
  \item exact frequencies of content words can help to automatically determine
        the topic, genre, sentiment etc. of a text
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{tf--idf (1/2)}
 \begin{itemize}
  \item task: given a word and a collection of documents, for which documents
        is this word most characteristic?
  \item e.g. Web search engine: word = search term, collection of documents =
        all Web pages
  \item roughly, we want to find documents in which the search term is
        significantly more frequent than in the whole collection
        $\rightarrow$ find only relevant documents
  \item tf--idf is a widely used formula for ``calculating the relevance'' of a
        document $d$ in a collection $D$ for a word $t$.
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{tf--idf (2/2)}
 \begin{block}{term frequency (tf)}
  How often does $t$ appear in $d$, relative to the frequency of the most
  frequent word in $d$?\\\center
  $\mathrm{tf}(t,d)=\frac{\mathrm{f}(t,d)}{\mathrm{max}\{\mathrm{f}(w,d):w\in d\}}$
 \end{block}\pause
 \begin{block}{inverse document frequency (idf)}
  Does $t$ appear in many documents in $D$? If so, low idf -- but if it's rare,
  high idf.\\\center
  $\mathrm{idf}(t,D)=\mathrm{log}\frac{|D|}{|\{d\in D:t\in d\}|}$
 \end{block}\pause
 \begin{block}{tf--idf}
  Final ``relevance'' obtained by multiplying the two scores.\\\center
  $\mathrm{tfidf}(t,d,D)=\mathrm{tf}(t,d)\times\mathrm{idf}(t,D)$
 \end{block}
\end{frame}

\section{Indexing}

\subsection{Indexing}

\begin{frame}
 \frametitle{Indexing}
 \begin{itemize}
  \item imagine you have a collection of thousands of documents, say
        \texttt{Song} objects with a \texttt{lyrics} attribute (text)
  \item you want to search for songs whose lyrics contain a certain word
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Basic search}
 \begin{Verbatim}
word = input('Enter your search word: ')
for song in songs:
    if word in tokenize(song.get_lyrics()):
        print(song)
 \end{Verbatim}
 \pause
 \begin{itemize}
  \item this goes through \emph{all} words of \emph{all} songs for \emph{every}
        search -- quite inefficient for many songs
  \item imagine Google iterating over the whole Web for each query --
        unthinkable!
  \item it would be better if we could just \emph{look up} which songs contain
        our word in an index
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Search using an index}
 \begin{Verbatim}
word = input('Enter your search word: ')
for song in songs_by_word[word]:
    print(song)
 \end{Verbatim}
 \begin{itemize}
  \item using a dictionary as an index: \texttt{songs\_by\_word}
  \item iterate only over the songs we already know contain the word
  \item much more efficient
  \item but how do we build an index?
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Building an index}
 \begin{Verbatim}
songs_by_word = {}
for song in songs:
    words = tokenize(song.get_lyrics())
    for word in words:
        if word not in songs_by_word:
            songs_by_word[word] = []
        songs_by_word[word].append(song)
 \end{Verbatim}
 \begin{itemize}
  \item keys: words
  \item values: lists of songs (those containing the key word)
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{More on indexing}
 \begin{itemize}
  \item example showed indexing by words
  \item in applications that require search over a large number of objects,
        it is also common to index by other attributes, e.g. for \texttt{Song}
        objects:
  \begin{itemize}
   \item genre
   \item artist name
   \item album title
   \item ...
  \end{itemize}
  \item you end up with multiple indices
 \end{itemize}
\end{frame}

\section{References}

\subsection{References}

\begin{frame}
 \frametitle{References}
 \begin{itemize}
  \item \url{https://en.wikipedia.org/wiki/Unicode}
  \item \url{https://en.wikipedia.org/wiki/UTF-8}
  \item \url{http://www.fileformat.info/info/unicode/block/index.htm} --
        database of all Unicode characters
  \item \url{https://en.wikipedia.org/wiki/tf-idf}
 \end{itemize}
\end{frame}

\end{document}
