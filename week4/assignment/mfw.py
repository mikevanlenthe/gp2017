#!/usr/bin/env python3

import operator
import sys
from collections import defaultdict


def tokenize(string):
    string = string.lower()
    dirtylist = string.split()
    cleanlist = []
    clean = ""
    for word in dirtylist:

        for char in word:
            if char.isalpha() is False:
                char = ' '
                clean = clean + char
            else:
                clean = clean + char
        clean = clean + ' '

    cleanlist = clean.split()

    return cleanlist


def most_frequent_words(wordlist):
    frequency_defdict = defaultdict(int)
    for word in wordlist:
        frequency_defdict[word] += 1

    sorted_d = (sorted(frequency_defdict.items(),
                key=operator.itemgetter(1), reverse=True))

    ten_most_words = []
    for word, count in sorted_d[0:10]:
        tuple = (word, count)
        ten_most_words.append(tuple)

    return ten_most_words


def filter_stopwords(word_list):
    stopwords = []
    file_handle = open("stopwords.txt", "r")
    for line in file_handle:
        stopwords.append(line.rstrip())
    file_handle.close()

    filtered_word_list = []
    for token in word_list:
        if token not in stopwords:
            filtered_word_list.append(token)

    return filtered_word_list


if __name__ == '__main__':
    try:
        _, path = sys.argv
    except ValueError:
        print('USAGE: python3 mfw.py TEXTFILE', file=sys.stderr)
        sys.exit(1)
    with open(path) as f:
        tokens = filter_stopwords(tokenize(f.read()))
    mfw = most_frequent_words(tokens)
    for word, count in mfw:
        print(count, word)
