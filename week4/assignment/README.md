Assignment for Week 4
=====================

`mfw.py` is a program that takes a text file as input and displays the ten
words occurring most frequently in the text. `mfw.py` is give to you but it
isn’t complete yet. It needs functions to tokenize the text, to filter out
stopwords and to count words. Your task is to write these functions.

Step 1: Get the Starter Code
----------------------------

On the command line, go into your local copy of the course repository. Update
it using the following command.

    git pull

This adds a directory `week4`, containing slides and assignment files.

In `week4/assignment`, you will find a subdirectory called `documents`. It
contains a small sample of five novels from
[Project Gutenberg](https://www.gutenberg.org): Alice in Wonderland, Jane Eyre,
Moby Dick, Pride and Prejudice and Twenty Thousand Leagues Under the Sea. You
will also find the incomplete `mfw.py`.

Step 2: Implement a Simple Tokenizer (4 Points)
-----------------------------------------------

Add a function `tokenize`. It should take a string as argument and return the
words in this string as a list.  Punctuation should be discarded for this
exercise. You can do this in the following way:

1. Convert the input string to lower case
2. Replace every non-alphabetic character in the string by a space `' '` (hint:
   use the [isalpha](https://docs.python.org/3/library/stdtypes.html?highlight=isalpha#str.isalpha)
   method)
3. Use the [split](https://docs.python.org/3/library/stdtypes.html?highlight=isalpha#str.split)
   method to get the list of words separated by whitespace

Step 3: Find the Ten Most Frequent Words in the Corpus (2 Points)
-----------------------------------------------------------------

Add a function `most_frequent_words`. It should take a list of words as
argument, count how often each word occurs in the list and return the ten most
frequent words in the list together with their counts. Return them as a list of
word-count pairs, sorted by descending frequency.

Step 4: Test the Program
------------------------

`mfw.py` should now be functional. Call it from the command line, giving the
text files in the `documents` directory as arguments. You will find that the
output does not tell you much about the contents of the files because it mostly
consists of function words like _the_ or _was_. We will solve this problem in
the next step.

Step 5: Filter Stopwords (4 Points)
-----------------------------------

Add a function `filter_stopwords`. It should take a list of words as argument
and return the list without stopwords. Stopwords are frequent words that do not
say much about the content of a document, e.g. function words such as _is_,
_the_ or _a_. They are uninteresting for many text processing applications and
are therefore often filtered out. Use `stopwords.txt` to determine which words
are stopwords.

Adapt the main code of `mfw.py` to use the stopword filter and run the program
on the text files again. You will see that the output is now quite a bit more
meaningful and distinctive.

Step 6: Run the Unit Tests
--------------------------

From the `assignment` directory, run

    $ python3 -m unittest

The unit tests test each of the three functions in isolation, so even if you did
not get the program to fully work, you may see some tests pass and some tests
fail.

Try to improve your program until all tests pass. Note that additional unit
tests may be used in the end to grade your work.

Step 7: Submit Your Work
------------------------

From the `week4/assignment` directory, run `git status` to see all the files
you modified and created. Add them all and commit the changes with a
meaningful message. Push them to your private repository using this command:

    git push mine master

Go to https://bitbucket.org/USERNAME/gp2017/src/master/week4/assignment/
(substituting your own username) and check the files to verify all your
changes have arrived (you may have to login to BitBucket first).

Finally, submit via Nestor. Again, you do not have to attach any files, just
submit the URL of your private BitBucket repository.

Grading
-------

Homework is graded on Tuesdays and Thursdays.

You may submit up to two attempts. The one with the better grade is counted.

You are strongly encouraged to submit a first attempt by
*Thursday, 9 March 2017, 15:00*. This way, you will receive a first grade
on the same day and still have time to submit another attempt if desired.

The deadline for submitting attempts is *Tuesday, 14 March 2017, 14:55*.

*Happy hacking!*
