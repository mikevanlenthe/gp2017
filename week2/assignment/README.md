Assignment for week 2
=====================

In this assignment you will apply the concept of inheritance and the
model-view-controller pattern. You will also delve back into GUI programming
using `graphics.py` (which most of you know from _Inleiding programmeren_) and
scratch the surface of event-driven programming.

Step 1: Get the starter code
----------------------------

On the command line, go into your `gp2017` directory, i.e. your local copy of
the course repository that you created last week. Update it using the following
command.

    git pull

This adds a directory `week2`, containing slides and assignment files. In
`week2/assignment`, you will find a program `switches.py`.

Run it and see what it does: it simulates a box with two switches and one
lightbulb. The user can flick the switches to ON or OFF, and the lightbulb is
ON when *both* switches are ON. The interface is text-based.

Open the program and see how it is structured: it uses

* a *model* class `AndSwitchesModel`, which saves the state of the switches and
  decides whether the lightbulb is ON or OFF,
* a *view* class, which displays the current model state to the user and
  prompts her for the next command,
* a *controller* class with a loop that in each iteration first tells the view
  to display the current state and to prompt the user for the next command,
  then updates the model according to the command.

Here is a UML diagram:

![UML diagram of the Switches classes](switches-uml.png)

Step 2: Add another model class (3 points)
------------------------------------------

Open `switches_model.py`. The model class used by `switches.py` is
`AndSwitchesModel`, which is a subclass of `AbstractSwitchesModel`. The
contract between the controller and the model can be summarized as follows:

* The model provides getters `is_switch1_on` and `is_switch2_on`. They return
  the current state of the respective switch as a boolean value.
* The model provides setters `set_switch1` and `set_switch2`. The controller
  may call them with a boolean argument, and the model will accordingly update
  the state of the switches.
* The model provides the method `is_lightbulb_on`, which will always return the
  current state of the lightbulb when called.

Add an alternative model class `OrSwitchesModel` to the `switches_model` module
that also adheres to this contract, but behaves differently from
`AndSwitchesModel`: the lightbulb is ON when *either* of the switches (or both)
is ON. This new class should also be a subclass of `AbstractSwitchesModel` and
it should only override the method(s) that need(s) to be overridden.

Change `switches.py` to use `OrSwitchesModel` instead of `AndSwitchesModel`.

Step 3: Add a GUI view class (6 points)
---------------------------------------

Open `switches_view.py`. The view class used by `switches.py` is
`TextSwitchesView`. The contract between the controller and the view can be
summarized as follows:

* The controller will call the view’s `show` method once, before the main loop
  starts. The view will then become visible without showing any particular
  model.
* The controller will call the view’s `hide` method once, after the main loop
  ends. The view will then disappear.
* The controller will call the view’s `display` method once in every iteration
  of the main loop, passing the model as argument. The view will then show the
  current state of the model to the user.
* The controller will call the view’s `get_action` method once in every
  iteration of the main loop. The view will return the action requested by the
  user as a string: `'flick_switch1'`, `'flick_switch2'` or `'quit'`, so that
  the controller may take the action.

Add an alternative view class `GUISwitchesView` to the `switches_view` module
that also adheres to this contract, but uses a graphical window instead of a
text based interface.

Change `switches.py` to use `GUISwitchesView` instead of `TextSwitchesView`.

Requirements:

* Use [`graphics.py`](http://mcsp.wartburg.edu/zelle/python/graphics/graphics/index.html)
  for the GUI
* Switches and the lightbulb are shown as pictures (Hint: you can use the
  `graphics.Image` class and the PNG files provided in the assignment
  directory)
* The user can flick the switches by clicking on them (Hint: in your view’s
  `get_action` method, use `getMouse` and the `graphics_util.is_within` method
  to determine whether the user clicked on an image, and which one)
* Only one GUI window is created and stays open for as long as the program
  runs. Its contents are updated in each iteration of the main loop. (Hint:
  use the `show`/`hide` methods of your view class to open/close the window.
  Store that window in an instance variable. Use the `display` method of your
  view class and the `undraw`/`draw` methods of the image objects to update the
  GUI.)
* When the user clicks somewhere where there is no image, nothing happens
* When the user closes the window, the program exits without an error message
  (Hint: the following code shows an example of how to avoid an error message
  when closing the window)

~~~
try:
    click = self.win.getMouse()
except GraphicsError:
    return 'quit'
~~~

Bonus step 4: Create an alternative implementation using `PyQt4` (1 point)
--------------------------------------------------------------------------

If you like a challenge and an extra point, implement the same application
differently, using the advanced `PyQt4` GUI library. You can check Johannes
Bjerva’s slides (see Nestor) for an introduction to `PyQt4`. `PyQt4` allows
you to connect GUI elements directly to the model and to update parts of the
GUI when they change without redrawing the entire window contents. You do
not have to stick to the existing contracts or classes for this – you can
create modified versions of the existing ones or implement your own from
scratch.

Name your alternative implementation `switches_pyqt.py`. Make sure that your
solutions to steps 2 and 3 stay intact.

Step 5: Run the unit tests
--------------------------

From the `week2/assignment` directory, run the following command:

    python3 -m unittest

If you completed steps 2 and 3, all tests should pass. Note that this only
does a superficial check that the required classes and methods are in the
right places. I will test the functionality and code more thoroughly for
grading, so make sure you fulfilled all the requirements (as far as possible).

Step 6: Submit your work
------------------------

From the `week2/assignment` directory, run `git status` to see all the files
you modified and created. Add them all. Commit the changes with a meaningful
message. Push them to your private repository using this command:

    git push mine master

Go to https://bitbucket.org/USERNAME/gp2017/src/master/week2/assignment/
(substituting your own username) and check the files to verify all your
changes have arrived (you may have to login to BitBucket first).

Finally, submit via Nestor. Again, you do not have to attach any files, just
submit the URL of your private BitBucket repository.

Grading
-------

Homework is graded on Tuesdays and Thursdays.

You may submit up to two attempts. The one with the better grade is counted.

You are strongly encouraged to submit a first attempt by
*Thursday, 23 February 2017, 15:00*. This way, you will receive a first grade
on the same day and still have time to submit another attempt if desired.

The deadline for submitting attempts is *Tuesday, 28 February 2017, 14:55*.

*Happy hacking!*
