class SwitchesController:

    def __init__(self, model, view):
        self.model = model
        self.view = view

    def run(self):
        self.view.show()
        while True:
            self.view.display(self.model)
            action = self.view.get_action()
            if action == 'flick_switch1':
                self.model.set_switch1(not self.model.is_switch1_on())
            elif action == 'flick_switch2':
                self.model.set_switch2(not self.model.is_switch2_on())
            elif action == 'quit':
               break
            else:
               raise Exception('Invalid action: {}'.format(action))
        self.view.hide()
