class AbstractSwitchesModel:

    def __init__(self):
        """
        Creates a new model with both switches in the off position.
        """
        self.switch1 = False
        self.switch2 = False

    def is_switch1_on(self):
        return self.switch1

    def is_switch2_on(self):
        return self.switch2

    def set_switch1(self, on):
        self.switch1 = on

    def set_switch2(self, on):
        self.switch2 = on

    def is_lightbulb_on(self):
        raise NotImplementedError()


class AndSwitchesModel(AbstractSwitchesModel):

    def is_lightbulb_on(self):
        return self.is_switch1_on() and self.is_switch2_on()


class OrSwitchesModel(AbstractSwitchesModel):

    def is_lightbulb_on(self):
        return self.is_switch1_on() or self.is_switch2_on()
