from graphics import *
from graphics_util import is_within


class TextSwitchesView:

    def show(self):
        pass

    def display(self, model):
        if model.is_switch1_on():
            string = '[ON ]'
        else:
            string = '[OFF]'
        print('{} switch 1'.format(string))
        if model.is_switch2_on():
            string = '[ON ]'
        else:
            string = '[OFF]'
        print('{} switch 2'.format(string))
        if model.is_lightbulb_on():
            string = '[ON ]'
        else:
            string = '[OFF]'
        print('{} lightbulb'.format(string))

    def get_action(self):
        print('Enter 1 to flick switch 1, 2 to flick switch 2, q to quit.')
        while True:
            command = input('> ')
            if command == '1':
                return 'flick_switch1'
            elif command == '2':
                return 'flick_switch2'
            elif command == 'q':
                return 'quit'
            else:
                print('Invalid command!')

    def hide(self):
        pass


class GUISwitchesView:

    def show(self):
        self.win = GraphWin("Mike van Lenthe - s3253481", 500, 500)
        self.image1on = Image(Point(200, 350), 'switch-on.png')
        self.image1off = Image(Point(200, 350), 'switch-off.png')
        self.image2on = Image(Point(300, 350), 'switch-on.png')
        self.image2off = Image(Point(300, 350), 'switch-off.png')
        self.lightbulbon = Image(Point(250, 150), 'lightbulb-on.png')
        self.lightbulboff = Image(Point(250, 150), 'lightbulb-off.png')

    def display(self, model):
        imagelist = [self.image1on, self.image1off, self.image2on,
                     self.image2off, self.lightbulbon, self.lightbulboff]

        for image in imagelist:
            try:
                image.undraw()
            except:
                pass

        if model.is_switch1_on():
            self.image1on.draw(self.win)
        else:
            self.image1off.draw(self.win)

        if model.is_switch2_on():
            self.image2on.draw(self.win)
        else:
            self.image2off.draw(self.win)

        if model.is_lightbulb_on():
            self.lightbulbon.draw(self.win)
        else:
            self.lightbulboff.draw(self.win)

    def get_action(self):
        while True:
            try:
                position = self.win.getMouse()
            except GraphicsError:
                return 'quit'

            if (is_within(position, self.image1on) or
                    is_within(position, self.image1off)) is True:
                return 'flick_switch1'

            elif (is_within(position, self.image2on) or
                  is_within(position, self.image2off)) is True:
                return 'flick_switch2'

    def hide(self):
        self.win.close()
