def is_within(point, image):
    """
    Takes a graphics.Point and a graphics.Image and returns a bool value
    indicating whether the point lies within the image.
    """
    anchor = image.getAnchor()
    width = image.getWidth()
    height = image.getHeight()
    return anchor.x - width / 2 <= point.x <= anchor.x + width / 2 \
        and anchor.y - height / 2 <= point.y <= anchor.y + height / 2
