#!/usr/bin/env python3


from switches_model import OrSwitchesModel
from switches_view import GUISwitchesView
from switches_controller import SwitchesController


SwitchesController(OrSwitchesModel(), GUISwitchesView()).run()
