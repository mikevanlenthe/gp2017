import json
import os
import re
import unittest


STUDENT_NUMBER_PATTERN = re.compile(r's\d{7}$')


class JSONTest(unittest.TestCase):

    def setUp(self):
        with open('me.json') as f:
            self.data = json.load(f)

    def test_name(self):
        self.assertIn('Name', self.data,
                      msg='Name property is missing')
        self.assertIsInstance(self.data['Name'], str,
                              msg='Value for Name is not a string')
        self.assertTrue(self.data['Name'].strip(),
                        msg='Value for Name is empty')

    def test_student_number(self):
        self.assertIn('Student number', self.data,
                      msg='Student number property is missing')
        self.assertIsInstance(self.data['Student number'], str,
                              msg='Value for Student number is not a string')
        match = STUDENT_NUMBER_PATTERN.match(self.data['Student number'])
        self.assertTrue(match,
                        msg='Value for Student number must be a lower-case s '
                            'followed by seven digits')

    def test_workgroup(self):
        self.assertIn('Workgroup', self.data,
                      msg='Workgroup property is missing')
        self.assertIsInstance(self.data['Workgroup'], int,
                              msg='Value for Workgroup must be an integer')
        self.assertIn(self.data['Workgroup'], (1, 2, 3),
                      msg='Value for Workgroup must be one of 1, 2 or 3')


class ImageTest(unittest.TestCase):

   def test_image(self):
       self.assertTrue(os.path.isfile('me.jpg') or os.path.isfile('me.png') or
                       os.path.isfile('me.gif'),
                       msg='No image me.jpg, me.png or me.gif found')


class TextTest(unittest.TestCase):

   def test_text(self):
       self.assertTrue(os.path.isfile('me.txt'),
                       msg='No file me.txt found')
       with open('me.txt') as f:
           text = f.read()
       self.assertTrue(text.strip(),
                       msg='me.txt is empty')  
