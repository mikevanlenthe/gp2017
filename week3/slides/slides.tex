\documentclass{beamer}

\usetheme{Singapore}

\usepackage{fancyvrb}
\usepackage{relsize}
\usepackage[utf8]{inputenc}

\title{Data Structures and Data Formats}
\author[Kilian Evang]{Kilian Evang\\
\quad\\
Gevorderd programmeren\\
Week 3, lecture}
\date{2017-02-21}

\AtBeginSection[]
{
    \begin{frame}{Outline}
        \tableofcontents[currentsection]
    \end{frame}
}

\AtBeginSubsection[]
{
    \begin{frame}{Outline}
        \tableofcontents[currentsection,currentsubsection]
    \end{frame}
}

\begin{document}

\begin{frame}
 \titlepage
\end{frame}

\begin{frame}
 \tableofcontents
\end{frame}

\section[Data Structures]{Working with Data Structures in Python}

\subsection[Recap]{Recap}

\begin{frame}
 \frametitle{Recap: Python Data Structure Types}
 \begin{itemize}
  \item Sequential collections
  \begin{itemize}
   \item \texttt{str}
   \item \texttt{tuple}
   \item \texttt{list}
  \end{itemize}
  \item Non-sequential collections
  \begin{itemize}
   \item \texttt{dict}
   \item \texttt{collections.Counter}
  \end{itemize}
 \end{itemize}\pause
 Zelle, ch. 11; slides of \emph{Inleiding programmeren II}
\end{frame}

\begin{frame}
 \frametitle{More Python Data Structure Types}
 \begin{itemize}
  \item Sequential collections
  \begin{itemize}
   \item \texttt{str}
   \item \texttt{tuple}
   \item \texttt{list}
  \end{itemize}
  \item Non-sequential collections
  \begin{itemize}
   \item \texttt{set} (new)
   \item \texttt{dict}
   \item \texttt{collections.defaultdict} (new)
   \item \texttt{collections.Counter}
  \end{itemize}
 \end{itemize}
\end{frame}

\subsection{Sets}

\begin{frame}[fragile]
 \frametitle{Sets}
 \begin{itemize}
  \item Can be seen as dictionaries with only keys, no values:
   \begin{itemize}
    \item are unordered
    \item items are unique
    \item items can be of any \emph{non-mutable} type
   \end{itemize}
   \item Efficient structures for e.g.\ membership testing
   \item Support set operators such as Union, Intersection and Difference
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Sets}
  \begin{block}{Example: Creating and Adding to a Set}
  \begin{Verbatim}[commandchars=\\\{\}]
>>> letters = \{'a', 'b', 'c'\}
>>> letters
\{'a', 'c', 'b'\}
>>> letters.add('d')
>>> letters
\{'d', 'a', 'c', 'b'\}
>>> letters.add('b')
>>> letters
\{'d', 'a', 'c', 'b'\}
  \end{Verbatim}
 \end{block}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Sets}
  \begin{block}{Example: Using Sets to Remove Duplicates}
  \begin{Verbatim}[commandchars=\\\{\}]
>>> letters = ['a', 'b', 'b', 'c', 'd', 'd']
>>> letters
['a', 'b', 'b', 'c', 'd', 'd']
>>> letters = set(letters)
>>> letters
\{'d', 'a', 'c', 'b'\}
  \end{Verbatim}
 \end{block}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Sets}
  \begin{block}{Example: Set Operations}
  \begin{Verbatim}[commandchars=\\\{\}]
>>> a = \{1,2,3,4\}
>>> b = \{3,4,5,6\}
>>> 4 in a              # Membership testing
True
>>> a & b               # Intersection
\{3,4\}
>>> a | b               # Union
\{1,2,3,4,5,6\}
>>> a - b               # Difference
\{1,2\}
>>> a ^ b               # Symmetric difference
\{1,2,5,6\}
  \end{Verbatim}
 \end{block}
\end{frame}

\subsection{Defaultdict}

\begin{frame}[fragile]
 \frametitle{Defaultdict}
  \begin{itemize}
    \item Dictionary that takes a default value for each new key
    \pause
    \item Saves us from specifying this value when an item is added
    \item Saves us from checking whether items are already in the dict
    \pause
    \item The default value can be a data type (e.g.\ int, list)
    \item The default value can even be a (lambda) function
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \begin{block}{Example 1 - Word frequency counting}
  \begin{Verbatim}[commandchars=\\\{\}]
>>> from collections import defaultdict
>>> words = [...]       # Some list of words
>>> ...\pause
>>> frequency_dict = \{\}
>>> for word in words:
        if word not in frequency_dict:  # Check existense
            frequency_dict[word] = 1
        else:
            frequency_dict[word] += 1
>>> ...\pause
>>> frequency_defdict = defaultdict(int)
>>> for word in words:
        frequency_defdict[word] += 1
>>>
  \end{Verbatim}
 \end{block}
\end{frame}

\begin{frame}[fragile]
  \begin{block}{Example 2 - Assigning word IDs}
  \begin{Verbatim}[commandchars=\\\{\}]
>>> from collections import defaultdict
>>> words = [...]       # Some list of words
>>> ...\pause
>>> word_id_dict = \{\}
>>> for word in words:
        if word not in word_id_dict:   # Check existense
            word_id_dict[word] = len(word_id_dict)
>>> ...\pause
>>> word_id_defdict = defaultdict(
                        lambda: len(word_id_defdict))
>>> for word in words:
        word_id_defdict[word]
>>>
  \end{Verbatim}
 \end{block}
\end{frame}

\section{List Comprehensions}

\subsection{List Comprehensions}

\begin{frame}
 \frametitle{Motivation}
 \begin{itemize}
  \item We often need to perform an operation on each element of a sequence to get a new sequence
  \item Plain solution: create a new list, write a \texttt{for} loop -- cumbersome
  \item Python's list comprehensions allow to write this more concisely
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Example: compute the list of the first five nonnegative square numbers}
 \begin{block}{With a Loop}
  \begin{Verbatim}
squares = []
for i in range(5):
    squares.append(i * i)
  \end{Verbatim}
 \end{block}
 \pause
 \begin{block}{With a List Comprehension}
  \begin{Verbatim}
squares = [i * i for i in range(5)]
  \end{Verbatim}
 \end{block}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Example: from a list of \texttt{Student} objects, make list of student numbers}
 \begin{block}{With a Loop}
  \begin{Verbatim}
student_numbers = []
for student in students:
    student_numbers.append(student.student_number)
  \end{Verbatim}
 \end{block}
 \pause
 \begin{block}{With a List Comprehension}
  \begin{Verbatim}
student_numbers = \
    [student.student_number for student in students]
  \end{Verbatim}
 \end{block}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Example: double each element that is greater than 4, discard the others}
 \begin{block}{With a Loop}
  \begin{Verbatim}
old = [3, 6, 2, 7, 1, 9]
new = []
for elem in old:
    if elem > 4:
        new.append(elem * 2) 
  \end{Verbatim}
 \end{block}
 \pause
 \begin{block}{With a Filtered List Comprehension}
  \begin{Verbatim}
old = [3, 6, 2, 7, 1, 9]
new = [elem * 2 for elem in old if elem > 4]
  \end{Verbatim}
 \end{block}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Example: in a list of dictionaries, discard those that don't have an \texttt{'answer'} key}
 \begin{block}{With a Loop}
  \begin{Verbatim}
quiz_filtered = []
for qanda in quiz:
    if 'answer' in qanda:
        quiz_filterd.append(qanda)
  \end{Verbatim}
 \end{block}
 \pause
 \begin{block}{With a Filtered List Comprehension}
  \begin{Verbatim}
quiz_filtered = \
    [qanda for quanda in quiz if 'answer' in qanda]
  \end{Verbatim}
 \end{block}
\end{frame}

\begin{frame}
 \frametitle{General Form of a List Comprehension}
 \texttt{[expression for name in collection]}

 \begin{itemize}
  \item where \texttt{expression} is some calculation or operation acting upon the variable \texttt{name}.
  \item For each member of the \texttt{collection}, the list comprehension
  \begin{itemize}
   \item sets \texttt{name} to that member,
   \item calculcates a new value using \texttt{expression}.
  \end{itemize}
  \item It then collects these new values into a list which is the return value of the list comprehension
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{General Form of a Filtered List Comprehension}
 \texttt{[expression for name in list if filter]}

 \begin{itemize}
  \item For each element of \texttt{list}, checks if it satisifies the \texttt{filter} condition
  \item If the \texttt{filter} condition returns \texttt{False}, that element is omitted from the list before the \texttt{expression} is evaluated
 \end{itemize}
\end{frame}

\section{Data Formats}

\subsection{Introduction}

\begin{frame}
 \frametitle{Data Formats: Why?}
 \begin{itemize}
  \item Save data to files, load them again
  \item Exchange data between different programs/programming languages/computers/operating systems...
  \item Communicate with Web services
  \item Exchange data between humans
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Data Formats: How?}
 \begin{itemize}
  \item Using a standardized format makes reading/writing easier, avoids re-inventing the wheel
  \item Formats that are both human-readable and machine-readable: XML, JSON...
 \end{itemize}
\end{frame}

\subsection{XML}

\begin{frame}
 \frametitle{XML: Overview}
 \begin{columns}
  \column{.5\textwidth}
  \begin{itemize}
   \item Extensible Markup Language
   \item Designed for text with markup (e.g. XHTML)
   \item Also widely used as a format for structured data
  \end{itemize}
  \column{.5\textwidth}
  \includegraphics[width=\textwidth]{XML.pdf}
 \end{columns} 
 \footnotesize Image credit: \url{https://en.wikipedia.org/wiki/XML}
\end{frame}

\begin{frame}
 \frametitle{XML: Terminology}
 \begin{columns}
  \column{.5\textwidth}
  \begin{itemize}
   \item Declaration
   \item Start tag, e.g. \texttt{<question>}
   \item End tag, e.g. \texttt{</question>}
   \item Element, e.g. \texttt{<greeting>Hello, world.</greeting>}
   \item Root element, here: \texttt{<quiz>...</quiz>}
   \item Attribute, e.g. \texttt{seq="1"}
   \item Comment: \texttt{<!-- ... -->}
  \end{itemize}
  \column{.5\textwidth}
  \includegraphics[width=\textwidth]{XML.pdf}
 \end{columns} 
\end{frame}

\begin{frame}[fragile]
 \frametitle{Example: reading XML with \texttt{xml.etree.ElementTree}}
 \begin{Verbatim}
import xml.etree.ElementTree as ET
tree = ET.parse('quiz.xml')
quiz = tree.getroot()
 \end{Verbatim}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Example: iterate through XML tree, print all questions}
 \begin{Verbatim}
for qanda in quiz:
    number = int(qanda.attrib['seq'])
    question = qanda.find('question').text
    print('{}. {}'.format(number, question))
 \end{Verbatim}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Example: delete questions that are missing an answer}
 \begin{Verbatim}
for qanda in quiz:
    answer = qanda.find('answer')
    if answer == None:
        quiz.remove(qanda)
 \end{Verbatim}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Example: write modified XML tree to file}
 \begin{Verbatim}
tree.write('quiz_filtered.xml')
 \end{Verbatim}
\end{frame}

\begin{frame}
 \frametitle{More about \texttt{xml.etree.ElementTree}}
 \url{https://docs.python.org/3.4/library/xml.etree.elementtree.html}
\end{frame}

\subsection{JSON}

\begin{frame}
 \frametitle{JSON: Overview}
 \begin{itemize}
  \item JavaScript Object Notation
  \item Many programming languages can read and write it easily
  \item Not suitable for text with markup
  \item More lightweight than XML for structured data
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{JSON: Datatypes}
 6 basic types
 \begin{itemize}
  \item Number
  \item String
  \item Boolean
  \item Array ($\cong$ \texttt{list})
  \item Object ($\cong$ \texttt{dict})
  \item null ($\cong$ \texttt{None})
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{JSON: Example}
 \footnotesize
 \begin{Verbatim}
[
    {
        "answer": "William Jefferson Clinton",
        "question": "Who was the forty-second president of the U.S.A.?",
        "seq": 1
    },
    {
        "answer": "Antananarivo",
        "question": "What is the capital of Madagascar?",
        "seq": 2
    }
]
 \end{Verbatim}
\end{frame}

\begin{frame}
 \frametitle{JSON and Python}
 \begin{itemize}
  \item JSON datatypes correspond directly to Python datatypes $\rightarrow$ very straightforward mapping
  \item Syntax similar, but not the same, e.g.: JSON does not allow single quotes for strings
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{JSON and Python: Examples}
 \begin{Verbatim}
import json

# Read from file
with open('data.json') as f:
    data = json.load(f)

# Write to file
with open('data.json', 'w') as f:
    json.dump(data, f)

# Read from string
data = json.loads('[{"x": 3}]')

# Write to string
json_string = json.dumps([{'x': 3]})
 \end{Verbatim}
\end{frame}

\begin{frame}
 \frametitle{More about \texttt{json}}
 \url{https://docs.python.org/3/library/json.html}
\end{frame}

\section{References}

\subsection{References}

\begin{frame}
 \frametitle{References}
 \begin{itemize}
  \item Some slides adapted from \url{http://www.csee.umbc.edu/courses/graduate/671/
fall13/notes/python/05python_comprehensions.ppt}
  \item Some slides by Johannes Bjerva
 \end{itemize}
\end{frame}

\end{document}
