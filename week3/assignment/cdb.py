import xml.etree.ElementTree as ET


def get_verbs(path_xml):
    tree = ET.parse(path_xml)
    cdbid = tree.getroot()

    cdbid_filtered = set(
        [cid.attrib['form'] for cid in cdbid if cid.attrib['pos'] == "VERB"])

    return cdbid_filtered


print(get_verbs('cdb-sample.xml'))
