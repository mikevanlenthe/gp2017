Assignment for week 3
=====================

In this assignment you will work with XML, JSON, strings and various data
structures.

Step 1: Get the starter code
----------------------------

On the command line, go into your local copy of the course repository. Update
it using the following command.

    git pull

This adds a directory `week3`, containing slides and assignment files.

Go into the directory `week3/assignment`.

Step 2: Finding Verbs (4.5 Points)
----------------------------------

Cornetto is a digital lexical database for Dutch. It is available in
`/net/corpora/Cornetto2.0`. The assignment directory contains a small sample of
Cornetto’s word list in the file `cdb-sample.xml`.

Create a Python module `cdb` with a function `get_verbs`. The function takes
the path to a Cornetto database XML file such as `cdb-sample.xml` or
`/net/corpora/Cornetto2.0/cdb2.0.id.xml`. It returns a collection of verbs in
the database.

Requirements:

* Use `xml.etree.ElementTree` to read the XML file and iterate over its `cid`
  elements. Each element corresponds to a word.
* The returned collection is a collection of Python strings. It contains the
  values of the `form` attributes of `cid` elements.
* In the returned collection, include only verbs. `cid` elements corresponding
  to verbs have `VERB` as the value of their `pos` (part of speech) attribute.
* Use a filtered list comprehension to filter out the non-verbs.
* Some words have duplicate entries. Make sure the returned collection contains
  each verb only once.

Step 3: Solving Word Search Puzzles (4.5 Points)
------------------------------------------------

Create a module `wordsearch` with a function `solve`. The function takes the path
to a text file with a word search puzzle, e.g. `puzzle1.txt`:

    WMELKXIS
    WHWBRZDA
    SESIXUTN
    RIBAEHDI
    EFBCETKS
    TFFEHDLN
    AOSISSAC
    WKFQTSKQ

The function returns a list of all words hidden in rows (from left to right) and
columns (from top to bottom). For example, for the above puzzle, it should
return `['MELK', 'KLAK']`.

In order to do find the words, your program needs a list of possible words.
Use the list in `words.txt` for this.

Bonus Step 4: Advanced Word Search (1 Point)
--------------------------------------------

Add a function `solve_advanced` to the `wordsearch` module. It should do the
same, but also find words that run backwards, i.e., from right to left or from
bottom top top. For the above puzzle, it should return
`['MELK', 'KLEM', 'CASSIS', 'KLAK', 'WATER', 'KOFFIE', 'KALK', 'INAS', 'SINAS']`.

No unit test is provided for the bonus step, you have to test it manually or by
writing your own unit test.

Step 5: Run the Unit Tests
--------------------------

From the `week3/assignment` directory, run the following command:

    python3 -m unittest

If you completed steps 2–3, all tests should pass. Note that additional tests
may be used to grade your work.

Step 8: Submit your work
------------------------

From the `week3/assignment` directory, run `git status` to see all the files
you modified and created. There should be two untracked files: `cdb.py` and
`wordsearch.py`. `git add` them. Commit the changes with a meaningful message.
Push them to your private repository using this command:

    git push mine master

Go to https://bitbucket.org/USERNAME/gp2017/src/master/week3/assignment/
(substituting your own username) and check the files to verify all your
changes have arrived (you may have to login to BitBucket first).

Finally, submit via Nestor. Again, you do not have to attach any files, just
submit the URL of your private BitBucket repository.

Grading
-------

Homework is graded on Tuesdays and Thursdays.

You may submit up to two attempts. The one with the better grade is counted.

You are strongly encouraged to submit a first attempt by
*Thursday, 2 March 2017, 15:00*. This way, you will receive a first grade
on the same day and still have time to submit another attempt if desired.

The deadline for submitting attempts is *Tuesday, 7 March 2017, 14:55*.

*Happy hacking!*
