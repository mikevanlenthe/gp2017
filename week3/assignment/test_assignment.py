import cdb
import unittest
import wordsearch


class CDBTest(unittest.TestCase):

    def test_verbs_1(self):
        verbs = list(cdb.get_verbs('cdb-sample.xml'))
        self.assertEqual(len(verbs), 105)
        self.assertEqual(set(verbs), {'', 'aanslepen', 'sturen', 'uitlopen', 'inhouden', 'monteren', 'ademen', 'aankunnen', 'inspringen', 'verbinden', 'overgaan', 'laten', 'afkomen', 'overstappen', 'zuigen', 'verlopen', 'klimmen', 'ophalen', 'volgen', 'verlammen', 'leggen', 'attenderen', 'afjagen', 'verkopen', 'doen', 'vermaken', 'oppompen', 'afstrijken', 'verrijden', 'uitvallen', 'vallen', 'inlopen', 'dragen', 'zagen', 'uitkomen', 'blazen', 'verspreiden', 'moeten', 'vechten', 'draaien', 'krijgen', 'aanschaffen', 'ophebben', 'aftekenen', 'inzakken', 'liggen', 'aanvaarden', 'doorgaan', 'laden', 'aanmoedigen', 'doorsteken', 'aanslaan', 'sterven', 'terugkomen', 'afsluiten', 'verteren', 'maken', 'uittrekken', 'rijden', 'starten', 'afspelen', 'vasthouden', 'mogen', 'afvliegen', 'doorkrijgen', 'lezen', 'verenigen', 'groeperen', 'ontspannen', 'kleuren', 'komen', 'beschouwen', 'schuiven', 'wegtrekken', 'oppassen', 'communiceren', 'lijden', 'halen', 'afhangen', 'springen', 'afscheiden', 'uitleggen', 'dienen', 'doordringen', 'groeien', 'afdoen', 'surfen', 'bekomen', 'zuiveren', 'opnemen', 'steunen', 'aanhouden', 'vastlopen', 'afspringen', 'kunnen', 'drijven', 'verheffen', 'verzekeren', 'doordraaien', 'ingaan', 'doortrappen', 'intrappen', 'verzuipen', 'scharrelen', 'voldoen'})


class WordSearchTest(unittest.TestCase):

    def test_wordsearch_1(self):
        words = list(wordsearch.solve('puzzle1.txt'))
        self.assertEqual(len(words), 2)
        self.assertEqual(set(words), {'KLAK', 'MELK'})

    def test_wordsearch_2(self):
        words = list(wordsearch.solve('puzzle2.txt'))
        self.assertEqual(len(words), 27)
        self.assertEqual(set(words), {'OVER', 'VIER', 'RAIO', 'NFWO', 'KAAS', 'GETROFFEN', 'NEGEN', 'TIEN', 'ZONNEBRIL', 'BRIL', 'GALMEN', 'GALM', 'MENISCUS', 'NAGALMEN', 'BABYUITZET', 'BABY', 'UITZET', 'OMNIBUS', 'STIL', 'STILTE', 'AFFAIRE', 'FAIR', 'ZEKER', 'HERSENS', 'EDEL', 'EDELE', 'BEDSTEDE'})
