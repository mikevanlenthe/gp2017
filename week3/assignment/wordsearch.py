import json
from collections import defaultdict


def solve(puzzle):
    with open('words.json') as f:
        data = json.load(f)

    puzzle_words = []
    file_handle = open(puzzle, "r")
    for line in file_handle:
        puzzle_words.append(line.rstrip())
    file_handle.close()

    puzzle_words2 = []
    for num in range(len(puzzle_words)):
        string = ""
        for line in puzzle_words:
            string = string + line[num]
        puzzle_words2.append(string)

    words_found = []
    for line in puzzle_words:
        for word in data["words"]:
            word = word.upper()
            if word in line:
                words_found.append(word)

    for line in puzzle_words2:
        for word in data["words"]:
            word = word.upper()
            if word in line:
                words_found.append(word)

    return words_found


def solve_advanced(puzzle):

    with open('words.json') as f:
        data = json.load(f)

    puzzle_words = []
    file_handle = open(puzzle, "r")
    for line in file_handle:
        puzzle_words.append(line.rstrip())

    puzzle_words_lr = []
    file_handle = open(puzzle, "r")
    for line in file_handle:
        line = line.rstrip()
        rev_line = line[::-1]
        puzzle_words_lr.append(rev_line.rstrip())
    file_handle.close()

    puzzle_words2 = []
    for num in range(len(puzzle_words)):
        string = ""
        for line in puzzle_words:
            string = string + line[num]
        puzzle_words2.append(string)

    puzzle_words_bt = []
    for num in range(len(puzzle_words)):
        string = ""
        for line in puzzle_words:
            string = string + line[num]
        puzzle_words_bt.append(string[::-1])

    words_found_advanced = []
    for line in puzzle_words:
        for word in data["words"]:
            word = word.upper()
            if word in line:
                words_found_advanced.append(word)

    for line in puzzle_words2:
        for word in data["words"]:
            word = word.upper()
            if word in line:
                words_found_advanced.append(word)

    for line in puzzle_words_lr:
        for word in data["words"]:
            word = word.upper()
            if word in line:
                words_found_advanced.append(word)

    for line in puzzle_words_bt:
        for word in data["words"]:
            word = word.upper()
            if word in line:
                words_found_advanced.append(word)

    return words_found_advanced

print(solve_advanced('puzzle1.txt'))
